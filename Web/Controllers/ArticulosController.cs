﻿using Datos;
using Entidades.Almacen;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.Almacen.Articulo;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticulosController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public ArticulosController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/Articulos
        [HttpGet("[action]")]
        public async Task<IEnumerable<ArticuloViewModel>> Listar()
        {
            var articulos = await _context.Articulos.Include(a => a.categoria).ToListAsync();
            return articulos.Select(a => new ArticuloViewModel
            {
                idarticulo = a.idarticulo,
                activo = a.activo,
                codigo = a.codigo,
                descripcion = a.descripcion,
                idcategoria = a.idcategoria,
                categoria = a.categoria.nombre,
                nombre = a.nombre,
                precio_venta = a.precio_venta,
                stock = a.stock
            });
        }

        // GET: api/Articulos/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var articulo = await _context.Articulos.Include(a=>a.categoria).SingleOrDefaultAsync(a=>a.idarticulo == id);

            if (articulo == null)
            {
                return NotFound();
            }

            return Ok(new ArticuloViewModel
            {
                idarticulo = articulo.idarticulo,
                activo = articulo.activo,
                codigo = articulo.codigo,
                descripcion = articulo.descripcion,
                idcategoria = articulo.idcategoria,
                categoria = articulo.categoria.nombre,
                nombre = articulo.nombre,
                precio_venta = articulo.precio_venta,
                stock = articulo.stock
            });
        }

        // PUT: api/Articulos/5
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idarticulo <= 0)
            {
                return BadRequest();
            }

            var articulo = await _context.Articulos.FirstOrDefaultAsync(c => c.idarticulo == model.idarticulo);

            if (articulo == null)
            {
                return NotFound();
            }


            articulo.codigo = model.codigo;
            articulo.descripcion = model.descripcion;
            articulo.idcategoria = model.idcategoria;
            articulo.nombre = model.nombre;
            articulo.precio_venta = model.precio_venta;
            articulo.stock = model.stock;


            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }

            return NoContent();
        }

        // POST: api/Articulos
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Articulo articulo = new Articulo
            {
                activo = true,
                codigo = model.codigo,
                descripcion = model.descripcion,
                idcategoria = model.idcategoria,
                nombre = model.nombre,
                precio_venta = model.precio_venta,
                stock = model.stock
            };

            _context.Articulos.Add(articulo);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {

                return BadRequest();
            }
            //return CreatedAtAction("GetArticulo", new { id = articulo.idarticulo }, articulo);
            return Ok();
        }

        // DELETE: api/Articulos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var articulo = await _context.Articulos.FindAsync(id);
            if (articulo == null)
            {
                return NotFound();
            }

            _context.Articulos.Remove(articulo);
            await _context.SaveChangesAsync();

            return Ok(articulo);
        }




        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> ActivarDesactivar([FromRoute] int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var articulo = await _context.Articulos.FirstOrDefaultAsync(c => c.idarticulo == id);


            if (articulo == null)
            {
                return NotFound();
            }

            articulo.activo = !articulo.activo;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar excepción
                return BadRequest();
            }

            return NoContent();
        }



        private bool ArticuloExists(int id)
        {
            return _context.Articulos.Any(e => e.idarticulo == id);
        }
    }
}